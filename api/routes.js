let router = require("express").Router();
let ganache = require("ganache-cli");

const Web3 = require("web3");
//const web3 = new Web3(ganache.provider());
const web3 = new Web3("http://127.0.0.1:7545");
//const web3 = new Web3("http://192.169.254.11:7545");

// contract ABI
const mcoin = require("../ethereum/build/MCoin.json");
const ptok = require("../ethereum/build/PTok.json");
const participant = require("../ethereum/build/Participant.json");
const emarket = require("../ethereum/build/EnergyMarket.json");
const db = require("./dbcon");

// contract address local
const mcoinAddr = "0x57996a25A5ebF6Ee316727EE866921065413b641";
const ptokAddr = "0x156D9210cCdaDABA91Af492E3c191e162964cc5D";
const participantAddr = "0x7e4671a6EB340B24743Aaf450CFABeaC5eA40Af8";
const emarketAddr = "0x5adF6E2ec21CC6f755772aC3644A4fe6EB54D9E2";

//const web3 = new Web3("http://192.169.254.11:7545");
/* const mcoinAddr = "0xdC74B528313bF40E11EA18CEe12B9C043083984C";
const ptokAddr = "0xdA622029f87Fa20733Ae00A583a3B493E417746d";
const participantAddr = "0x4c0361e3b64cDb949B42ca6512956a65d791313b";
const emarketAddr = "0xA33d92A2429D8F98f9CC82286b4720Cd7eba949e"; */

// Uncle Bear Ganache Addresses
/*const mcoinAddr = "0x02b99704E77eA3421D6E17dBAfFDd7AA1061970d";
const ptokAddr = "0x96340E1C406385B7070594D20aC9c444C415Db1c";
const participantAddr = "0xE7a97E1C32fa5dC290F8E994173ef5C1b162b44B";
const emarketAddr = "0x7CDDC186f2950618B620B9BB54448Ffe3D759809";*/

// load contracts
const Participant = new web3.eth.Contract(
  JSON.parse(participant.interface),
  participantAddr
);

const MCoin = new web3.eth.Contract(JSON.parse(mcoin.interface), mcoinAddr);

const PTok = new web3.eth.Contract(JSON.parse(ptok.interface), ptokAddr);

const Market = new web3.eth.Contract(
  JSON.parse(emarket.interface),
  emarketAddr
);

// API routes

/**********************************************************
 *                     ETHEREUM ACCOUNTS                  *
 *********************************************************/
router.get("/accounts", async (req, res) => {
  try {
    const accounts = await web3.eth.getAccounts();
    const accountDetails = await Promise.all(
      accounts.map(async (account, index) => {
        const balance = web3.utils.fromWei(
          await web3.eth.getBalance(account),
          "ether"
        );
        return {
          index: index,
          address: account,
          ether: balance
        };
      })
    );

    res.send(accountDetails);
  } catch (err) {
    res.status(400);
    res.send({
      success: false,
      message: err.message
    });
  }
});

router.get("/account/:addr", async (req, res) => {
  try {
    const coinbase = await web3.eth.getCoinbase();
    const address = req.params.addr || coinbase;
    const balance = await web3.eth.getBalance(address);
    res.send({
      address: address,
      ether: web3.utils.fromWei(balance, "ether")
    });
  } catch (err) {
    res.status(400);
    res.send({
      success: false,
      message: err.message
    });
  }
});

router.get("/blocks", async (req, res) => {
  try {
    const blockNumber = await web3.eth.getBlockNumber();
    const blocks = await Promise.all(
      Array(parseInt(blockNumber))
        .fill()
        .map(async (block, index) => {
          return await web3.eth.getBlock(index);
        })
    );

    res.send(blocks);
  } catch (err) {
    res.status(400);
    res.send({
      success: false,
      message: err.message
    });
  }
});

router.get("/set/:address", async (req, res) => {
  try {
    const address = req.params.address || "";

    if (!address) {
      throw new Error("Incorrect addresses");
    }
    web3.eth.defaultAccount = address;

    /*  await web3.eth.setEtherbase(address); */
    res.send({
      success: true,
      message: `Default account set to ${address}`
    });
  } catch (err) {
    res.status(400);
    res.send({
      success: false,
      message: err.message
    });
  }
});

/**********************************************************
 *                     REGISTRATIONS                      *
 *********************************************************/
/*------------------- POST /prosumers ---------------------*/
router.get("/prosumers", async (req, res) => {
  try {
    const coinbase = await web3.eth.getCoinbase();
    const prosumerAddrs = await Participant.methods
      .getProsumerAddresses()
      .call({ from: coinbase });

    const prosumerDetails = await Promise.all(
      prosumerAddrs.map(async addr => {
        // get prosumer details
        const prosumerDetail = await Participant.methods
          .getProsumerDetails(addr)
          .call({ from: coinbase, gas: "1000000" });

        // get ptok
        const ptok = await PTok.methods
          .balanceOf(addr)
          .call({ from: coinbase, gas: "1000000" });

        // get mcoin
        const mcoin = await MCoin.methods
          .balanceOf(addr)
          .call({ from: coinbase, gas: "1000000" });

        // get ether
        const balance = web3.utils.fromWei(
          await web3.eth.getBalance(addr),
          "ether"
        );

        return {
          name: prosumerDetail[0],
          account: addr,
          ptok: ptok,
          mcoin: mcoin,
          ether: balance,
          generatingCapacity: prosumerDetail[1],
          reliabilityRatings: prosumerDetail[2]
        };
      })
    );
    res.send(prosumerDetails);
  } catch (err) {
    res.status(400);
    res.send({
      success: false,
      message: err.message
    });
  }
});

/*------------------- GET /consumers ---------------------*/
router.get("/consumers", async (req, res) => {
  try {
    const coinbase = await web3.eth.getCoinbase();
    const consumerAddrs = await Participant.methods
      .getConsumerAddresses()
      .call({ from: coinbase });

    const consumerDetails = await Promise.all(
      consumerAddrs.map(async addr => {
        // get consumer details
        const consumerDetail = await Participant.methods
          .getConsumerDetails(addr)
          .call({ from: coinbase, gas: "1000000" });

        // get ptok
        const ptok = await PTok.methods
          .balanceOf(addr)
          .call({ from: coinbase, gas: "1000000" });

        // get mcoin
        const mcoin = await MCoin.methods
          .balanceOf(addr)
          .call({ from: coinbase, gas: "1000000" });

        // get ether
        const balance = web3.utils.fromWei(
          await web3.eth.getBalance(addr),
          "ether"
        );

        return {
          name: consumerDetail,
          account: addr,
          ptok: ptok,
          mcoin: mcoin,
          ether: balance
        };
      })
    );
    res.send(consumerDetails);
  } catch (err) {
    res.status(400);
    res.send({
      success: false,
      message: err.message
    });
  }
});

/*------------------- POST /prosumer ---------------------*/
router.post("/prosumer", async (req, res) => {
  const prosumer = {
    name: req.body.name || "",
    generatingCapacity: req.body.generatingCapacity || "",
    reliabilityRatings: req.body.reliabilityRatings || ""
  };

  try {
    if (
      !prosumer.name ||
      !prosumer.generatingCapacity ||
      !prosumer.reliabilityRatings
    ) {
      throw new Error("Required parameters missing");
    }

    const coinbase = await web3.eth.getCoinbase();
    await Participant.methods
      .enlistProsumer(
        prosumer.name,
        prosumer.generatingCapacity,
        prosumer.reliabilityRatings
      )
      .send({
        from: coinbase,
        gas: "1000000"
      });

    res.send({
      success: true,
      message: "Prosumer enlisted",
      data: prosumer
    });
  } catch (err) {
    res.status(400);
    res.send({
      success: false,
      message: err.message
    });
  }
});

/*------------------- POST /consumer ---------------------*/
router.post("/consumer", async (req, res) => {
  const consumer = {
    name: req.body.name || ""
  };

  try {
    if (!consumer.name) {
      throw new Error("Required parameters missing");
    }

    const coinbase = await web3.eth.getCoinbase();
    await Participant.methods.enlistConsumer(consumer.name).send({
      from: coinbase,
      gas: "1000000"
    });

    res.send({
      success: true,
      message: "Consumer enlisted",
      data: consumer
    });
  } catch (err) {
    res.status(400);
    res.send({
      success: false,
      message: err.message
    });
  }
});

/**********************************************************
 *                          MARKET                        *
 *********************************************************/

/*--------------- GET /energy/blocks --------------------*/
router.get("/energy/blocks", async (req, res) => {
  try {
    const blocks = await Market.methods.getEnergyBlockCount().call();
    const blockDetails = await Promise.all(
      Array(parseInt(blocks))
        .fill()
        .map(async (block, index) => {
          const blockDetail = await Market.methods.energyBlocks(index).call();
          const prosumerDetail = await Participant.methods
            .getProsumerDetails(blockDetail.prosumer)
            .call();
          const consumerDetail = await Participant.methods
            .getConsumerDetails(blockDetail.consumer)
            .call();
          return {
            energyId: index,
            prosumerAddress: blockDetail.prosumer,
            prosumerDetails: {
              name: prosumerDetail[0],
              generatingCapacity: prosumerDetail[1],
              reliabilityRatings: prosumerDetail[2]
            },
            consumerAddress: blockDetail.consumer,
            consumerDetails: {
              name: consumerDetail
            },
            timeStart: blockDetail.tStart,
            timePeriod: blockDetail.tPeriod,
            targetSupply: blockDetail.target,
            unitPrice: blockDetail.unitPrice,
            actualConsumed: blockDetail.actualConsumed,
            actualGenerated: blockDetail.actualGenerated,
            totalMCoinDeposited: blockDetail.deposited,
            totalMCoinPending: blockDetail.pendingPayment,
            status: blockDetail.status,
            state: blockDetail.state
          };
        })
    );
    res.send(blockDetails);
  } catch (err) {
    res.status(400);
    res.send({
      success: false,
      message: err.message
    });
  }
});

/*--------------- GET /energy/block/:id --------------------*/
router.get("/energy/block/:energyId", async (req, res) => {
  try {
    const index = req.params.energyId || "0";
    const blockDetail = await Market.methods.energyBlocks(index).call();
    const prosumerDetail = await Participant.methods
      .getProsumerDetails(blockDetail.prosumer)
      .call();
    const consumerDetail = await Participant.methods
      .getConsumerDetails(blockDetail.consumer)
      .call();

    res.send({
      energyId: index,
      prosumerAddress: blockDetail.prosumer,
      prosumerDetails: {
        name: prosumerDetail[0],
        generatingCapacity: prosumerDetail[1],
        reliabilityRatings: prosumerDetail[2]
      },
      consumerAddress: blockDetail.consumer,
      consumerDetails: {
        name: consumerDetail
      },
      timeStart: blockDetail.tStart,
      timePeriod: blockDetail.tPeriod,
      targetSupply: blockDetail.target,
      unitPrice: blockDetail.unitPrice,
      actualConsumed: blockDetail.actualConsumed,
      actualGenerated: blockDetail.actualGenerated,
      totalMCoinDeposited: blockDetail.deposited,
      totalMCoinPending: blockDetail.pendingPayment,
      status: blockDetail.status,
      state: blockDetail.state
    });
  } catch (err) {
    res.status(400);
    res.send({
      success: false,
      message: err.message
    });
  }
});

/*------------------- POST /energy/sell ---------------------*/
router.post("/energy/sell", async (req, res) => {
  try {
    const tStart = req.body.tStart || "";
    const tPeriod = req.body.tPeriod || "";
    const target = req.body.target || "";
    const unitPrice = req.body.unitPrice || "";

    if (!tStart || !tPeriod || !target || !unitPrice) {
      throw new Error("Required parameter missing");
    }

    const coinbase = await web3.eth.getCoinbase();
    await Market.methods.sellEnergy(tStart, tPeriod, target, unitPrice).send({
      from: req.body.address,
      gas: "1000000"
    });
    const id = parseInt(await Market.methods.getEnergyBlockCount().call()) - 1;
    var data = {
      id: req.body.id,
      address: req.body.address,
      data: {
        energyId: id,
        prosumer: coinbase,
        timeStart: tStart,
        timePeriod: tPeriod,
        targetSupply: target,
        unitPrice: unitPrice
      }
    };

    res.send({
      success: true,
      message: "Offer listed",
      data: {
        energyId: id,
        prosumer: coinbase,
        timeStart: tStart,
        timePeriod: tPeriod,
        targetSupply: target,
        unitPrice: unitPrice
      }
    });
  } catch (err) {
    res.status(400);
    res.send({
      success: false,
      message: err.message
    });
  }
});

/*------------------- POST /energy/buy ---------------------*/
router.post("/energy/buy", async (req, res) => {
  try {
    const coinbase = await web3.eth.getCoinbase();
    const id = req.body.energyId || "";

    if (!id) {
      throw new Error("Please check your energyId");
    }

    await Market.methods.buyEnergy(id).send({
      from: req.body.address,
      gas: "1000000"
    });

    const blockDetail = await Market.methods.energyBlocks(id).call();
    const prosumerDetail = await Participant.methods
      .getProsumerDetails(blockDetail.prosumer)
      .call();
    const consumerDetail = await Participant.methods
      .getConsumerDetails(blockDetail.consumer)
      .call();

    var data = {
      id: req.body.id,
      address: req.body.address,
      data: {
        energyId: id,
        prosumerAddress: blockDetail.prosumer,
        prosumerDetails: {
          name: prosumerDetail[0],
          generatingCapacity: prosumerDetail[1],
          reliabilityRatings: prosumerDetail[2]
        },
        consumerAddress: blockDetail.consumer,
        consumerDetails: {
          name: consumerDetail
        },
        timeStart: blockDetail.tStart,
        timePeriod: blockDetail.tPeriod,
        targetSupply: blockDetail.target,
        unitPrice: blockDetail.unitPrice,
        actualConsumed: blockDetail.actualConsumed,
        actualGenerated: blockDetail.actualGenerated,
        totalMCoinDeposited: blockDetail.deposited,
        totalMCoinPending: blockDetail.pendingPayment,
        status: blockDetail.status,
        state: blockDetail.state
      }
    };

    res.send({
      success: true,
      message: "Energy bought",
      data: {
        energyId: id,
        prosumerAddress: blockDetail.prosumer,
        prosumerDetails: {
          name: prosumerDetail[0],
          generatingCapacity: prosumerDetail[1],
          reliabilityRatings: prosumerDetail[2]
        },
        consumerAddress: blockDetail.consumer,
        consumerDetails: {
          name: consumerDetail
        },
        timeStart: blockDetail.tStart,
        timePeriod: blockDetail.tPeriod,
        targetSupply: blockDetail.target,
        unitPrice: blockDetail.unitPrice,
        actualConsumed: blockDetail.actualConsumed,
        actualGenerated: blockDetail.actualGenerated,
        totalMCoinDeposited: blockDetail.deposited,
        totalMCoinPending: blockDetail.pendingPayment,
        status: blockDetail.status,
        state: blockDetail.state
      }
    });
  } catch (err) {
    res.status(400);
    res.send({
      success: false,
      message: err.message
    });
  }
});

/*------------------- POST /energy/exchange ---------------------*/
router.post("/energy/exchange", async (req, res) => {
  try {
    const coinbase = await web3.eth.getCoinbase();
    const id = req.body.energyId || "";

    if (!id) {
      throw new Error("Required parameters missing");
    }

    await Market.methods.startExchanging(id).send({
      from: req.body.address,
      gas: "1000000"
    });
    const blockDetail = await Market.methods.energyBlocks(id).call();
    const prosumerDetail = await Participant.methods
      .getProsumerDetails(blockDetail.prosumer)
      .call();
    const consumerDetail = await Participant.methods
      .getConsumerDetails(blockDetail.consumer)
      .call();
    res.send({
      success: true,
      message: "Exchanging energy",
      data: {
        energyId: id,
        prosumerAddress: blockDetail.prosumer,
        prosumerDetails: {
          name: prosumerDetail[0],
          generatingCapacity: prosumerDetail[1],
          reliabilityRatings: prosumerDetail[2]
        },
        consumerAddress: blockDetail.consumer,
        consumerDetails: {
          name: consumerDetail
        },
        timeStart: blockDetail.tStart,
        timePeriod: blockDetail.tPeriod,
        targetSupply: blockDetail.target,
        unitPrice: blockDetail.unitPrice,
        actualConsumed: blockDetail.actualConsumed,
        actualGenerated: blockDetail.actualGenerated,
        totalMCoinDeposited: blockDetail.deposited,
        totalMCoinPending: blockDetail.pendingPayment,
        status: blockDetail.status,
        state: blockDetail.state
      }
    });
  } catch (err) {
    res.status(400);
    res.send({
      success: false,
      message: err.message
    });
  }
});

/*------------------- POST /energy/consumption ---------------------*/
router.post("/energy/consumption", async (req, res) => {
  try {
    const coinbase = await web3.eth.getCoinbase();

    const id = req.body.energyId || "";
    const consumption = req.body.consumption || "";

    if (!id || !consumption) {
      throw new Error("Required parameters missing");
    }

    await Market.methods.reportConsumed(id, consumption).send({
      from: req.body.address,
      gas: "1000000"
    });

    const blockDetail = await Market.methods.energyBlocks(id).call();
    const prosumerDetail = await Participant.methods
      .getProsumerDetails(blockDetail.prosumer)
      .call();
    const consumerDetail = await Participant.methods
      .getConsumerDetails(blockDetail.consumer)
      .call();

    res.send({
      success: true,
      message: "Actual consumption saved",
      data: {
        energyId: id,
        prosumerAddress: blockDetail.prosumer,
        prosumerDetails: {
          name: prosumerDetail[0],
          generatingCapacity: prosumerDetail[1],
          reliabilityRatings: prosumerDetail[2]
        },
        consumerAddress: blockDetail.consumer,
        consumerDetails: {
          name: consumerDetail
        },
        timeStart: blockDetail.tStart,
        timePeriod: blockDetail.tPeriod,
        targetSupply: blockDetail.target,
        unitPrice: blockDetail.unitPrice,
        actualConsumed: blockDetail.actualConsumed,
        actualGenerated: blockDetail.actualGenerated,
        totalMCoinDeposited: blockDetail.deposited,
        totalMCoinPending: blockDetail.pendingPayment,
        status: blockDetail.status,
        state: blockDetail.state
      }
    });
  } catch (err) {
    res.status(400);
    res.send({
      success: false,
      message: err.message
    });
  }
});

/*------------------- POST /energy/generation ---------------------*/
router.post("/energy/generation", async (req, res) => {
  try {
    const coinbase = await web3.eth.getCoinbase();

    const id = req.body.energyId || "";
    const generation = req.body.generation || "";

    if (!id || !generation) {
      throw new Error("Required parameters missing");
    }

    await Market.methods.reportGenerated(id, generation).send({
      from: req.body.address,
      gas: "1000000"
    });

    const blockDetail = await Market.methods.energyBlocks(id).call();
    const prosumerDetail = await Participant.methods
      .getProsumerDetails(blockDetail.prosumer)
      .call();
    const consumerDetail = await Participant.methods
      .getConsumerDetails(blockDetail.consumer)
      .call();

    res.send({
      success: true,
      message: "Actual generation saved",
      data: {
        energyId: id,
        prosumerAddress: blockDetail.prosumer,
        prosumerDetails: {
          name: prosumerDetail[0],
          generatingCapacity: prosumerDetail[1],
          reliabilityRatings: prosumerDetail[2]
        },
        consumerAddress: blockDetail.consumer,
        consumerDetails: {
          name: consumerDetail
        },
        timeStart: blockDetail.tStart,
        timePeriod: blockDetail.tPeriod,
        targetSupply: blockDetail.target,
        unitPrice: blockDetail.unitPrice,
        actualConsumed: blockDetail.actualConsumed,
        actualGenerated: blockDetail.actualGenerated,
        totalMCoinDeposited: blockDetail.deposited,
        totalMCoinPending: blockDetail.pendingPayment,
        status: blockDetail.status,
        state: blockDetail.state
      }
    });
  } catch (err) {
    res.status(400);
    res.send({
      success: false,
      message: err.message
    });
  }
});

/*------------------- POST /energy/settle ---------------------*/
router.post("/energy/settle", async (req, res) => {
  try {
    const coinbase = await web3.eth.getCoinbase();
    const accounts = await web3.eth.getAccounts();

    const id = req.body.energyId || "";

    if (!id) {
      throw new Error("Required parameters missing");
    }

    await Market.methods.settle(id).send({
      from: coinbase,
      gas: "1000000"
    });

    const blockDetail = await Market.methods.energyBlocks(id).call();
    const prosumerDetail = await Participant.methods
      .getProsumerDetails(blockDetail.prosumer)
      .call();
    const consumerDetail = await Participant.methods
      .getConsumerDetails(blockDetail.consumer)
      .call();

    res.send({
      success: true,
      message: "Energy settled",
      data: {
        energyId: id,
        prosumerAddress: blockDetail.prosumer,
        prosumerDetails: {
          name: prosumerDetail[0],
          generatingCapacity: prosumerDetail[1],
          reliabilityRatings: prosumerDetail[2]
        },
        consumerAddress: blockDetail.consumer,
        consumerDetails: {
          name: consumerDetail
        },
        timeStart: blockDetail.tStart,
        timePeriod: blockDetail.tPeriod,
        targetSupply: blockDetail.target,
        unitPrice: blockDetail.unitPrice,
        actualConsumed: blockDetail.actualConsumed,
        actualGenerated: blockDetail.actualGenerated,
        totalMCoinDeposited: blockDetail.deposited,
        totalMCoinPending: blockDetail.pendingPayment,
        status: blockDetail.status,
        state: blockDetail.state
      }
    });
  } catch (err) {
    res.status(400);
    res.send({
      success: false,
      message: err.message
    });
  }
});

/*------------------- GET /energy/events ---------------------*/
router.get("/energy/events", async (req, res) => {
  try {
    const filter = {
      fromBlock: 0,
      toBlock: "latest"
    };

    const events = await Market.getPastEvents("allEvents", filter);
    res.send(events);
  } catch (err) {
    res.status(400);
    res.send({
      success: false,
      message: err.message
    });
  }
});

router.get("/login", async (req, res) => {
  await db.Login(req.query.id, req.query.pass, function(err, result) {
    db.CreateSession("user", result[0]);
    res.send(result[0]);
  });
});

router.get("/authen", async (req, res) => {
  await db.GetAccount(req.query.id, function(err, result) {
    if (result.length > 0) {
      res.send(result[0]);
    }
  });
});

router.post("/getuserbyAddress", async (req, res) => {
  await db.GetUserbyAddress(req.body.address, function(err, result) {
    if (result.length > 0) {
      res.send(result[0]);
    } else {
      res.send(false);
    }
  });
});

router.post("/register", async (req, res) => {
  console.log(req);
  const coinbase = await web3.eth.getCoinbase();
  try {
    const accout = await db.Login(req.body.account, "", function(err, result) {
      if (result.length > 0) {
        res.send(false);
      } else {
        web3.eth.personal.newAccount(req.body.password, function(err, address) {
          //send ether
          //const sendeth = web3.eth.sendTransaction(coinbase,address,100, function(){
          var data = req.body;
          data.address = address;
          db.InsertAccount(data);
          res.send(true);
          //});
        });
      }
    });
  } catch (err) {
    res.status(400);
    res.send({
      success: false,
      message: err.message
    });
  }
});

module.exports = router;
