var MongoClient = require('mongodb').MongoClient;
var ObjectId = require('mongodb').ObjectID;
var url = "mongodb://localhost:27017/tradedb";

const express  = require('express');
const session = require('express-session');


module.exports =  {
  Login : function (acc,passw,callback) {
    MongoClient.connect(url, function(err, db) {
      if (err)  callback(err);       
      var dbo = db.db("tradedb");
      var param ;
      if(passw == '') {
        param = {'data.account' : acc};
      }
      else{
        param = { 'data.account' : acc , 'data.password' : passw};
      }
      var res;
      dbo.collection("accounts").find(param).toArray(function(err, result) {
        if (err)  callback(err);     
        db.close();
        callback(null,result);
      });    
    });
  },

  InsertAccount : function (obj) {
    MongoClient.connect(url, function(err, db) {
      if (err) throw err;
      var dbo = db.db("tradedb");
      var item = {data : obj}
      dbo.collection("accounts").insertOne(item, function(err, res) {
        if (err) throw callback(err);
        console.log("Insert accounts");
        callback(null,true);
      });
    });
  },
  InsertSellEnergy : function (obj,callback){
    MongoClient.connect(url, function(err, db) {
      if (err) throw err;
      var dbo = db.db("tradedb");
      dbo.collection("prosumers").insertOne(item, function(err, res) {
        if (err) throw err;
          console.log("Insert sell energy");
      });
    });
  },

  InsertBuyEnergy : function (obj,callback){
    MongoClient.connect(url, function(err, db) {
      if (err) throw err;
      var dbo = db.db("tradedb");
      dbo.collection("consumer").insertOne(item, function(err, res) {
        if (err) throw err;
          console.log("Insert buy energy");
      });
    });
  },

  GetAccount : function (id,callback) {
    MongoClient.connect(url, function(err, db) {
      if (err)  callback(err);       
      var dbo = db.db("tradedb");
      var param ;
      if(id != '') {
        var o_id = new ObjectId(id);
        param = { '_id' : o_id};
        dbo.collection("accounts").find(param).toArray(function(err, result) {
          if (err)  callback(err);     
          db.close();
          callback(null,result);
        });  
      }  
    });
  },
  GetUserbyAddress : function (addr,callback) {
    MongoClient.connect(url, function(err, db) {
      if (err)  callback(err);       
      var dbo = db.db("tradedb");
      var param ;
      if(addr != '') {
        param = { 'data.address' : addr};
        dbo.collection("accounts").find(param).toArray(function(err, result) {
          if (err)  callback(err);     
          db.close();
          callback(null,result);
        });  
      }  
    });
  },
  CreateSession : function (s_name,obj) {
      app = express();
      app.use(session({s_name: obj}));
  },

}