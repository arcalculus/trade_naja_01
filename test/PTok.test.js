const assert = require("assert");
const ganache = require("ganache-cli");

// use ganache virtual node instead of geth
const Web3 = require("web3");
//const web3 = new Web3(ganache.provider());
const web3 = new Web3("http://127.0.0.1:7545");

const { interface, bytecode } = require("../ethereum/build/PTok.json");

let PTok;
let accounts;

beforeEach(async () => {
  // get all accounts
  accounts = await web3.eth.getAccounts();

  // deploy contract for testing
  PTok = await new web3.eth.Contract(JSON.parse(interface))
    .deploy({
      data: bytecode
    })
    .send({
      from: accounts[0],
      gas: "1000000"
    });
});

describe("PreamTok Contract", () => {
  it("can deploy the PTok Contract", async () => {
    PTok = await new web3.eth.Contract(JSON.parse(interface))
      .deploy({
        data: bytecode
      })
      .send({
        from: accounts[0],
        gas: "1000000"
      });

    assert.ok(PTok.options.address);
  });

  it("can tranfer PTok from one account to another", async () => {
    await PTok.methods.generateEnergy(accounts[1], "1000").send({
      from: accounts[1],
      gas: "1000000"
    });

    await PTok.methods.transferFrom(accounts[1], accounts[2], "500").send({
      from: accounts[1],
      gas: "1000000"
    });

    const balanceA = await PTok.methods
      .balanceOf(accounts[1])
      .call({ from: accounts[1] });
    assert.strictEqual(balanceA, "500");

    const balanceB = await PTok.methods
      .balanceOf(accounts[2])
      .call({ from: accounts[2] });
    assert.strictEqual(balanceA, "500");
  });

  it("allows Latae the prosumer to generate energy", async () => {
    await PTok.methods.generateEnergy(accounts[1], "1000").send({
      from: accounts[1],
      gas: "1000000"
    });

    const myBalance = await PTok.methods.balanceOf(accounts[1]).call({
      from: accounts[1],
      gas: "1000000"
    });

    assert.strictEqual(myBalance, "1000");
  });

  it("allows Thames the consumer to use energy", async () => {
    // generate energy
    await PTok.methods.generateEnergy(accounts[1], "1000").send({
      from: accounts[1]
    });

    // transfer energy to another account
    await PTok.methods.transferFrom(accounts[1], accounts[2], "800").send({
      from: accounts[1]
    });

    // the consumer should have 800 PTok
    let consumerPTok = await PTok.methods
      .balanceOf(accounts[2])
      .call({ from: accounts[2] });
    assert.strictEqual(consumerPTok, "800");

    // the prosumer should have 200 PTok
    let prosumerPTok = await PTok.methods
      .balanceOf(accounts[1])
      .call({ from: accounts[1] });
    assert.strictEqual(prosumerPTok, "200");

    // the consumer uses 500 energy
    await PTok.methods.useEnergy(accounts[2], "500").send({
      from: accounts[2]
    });

    // the consumer should have 300 PTok left
    consumerPTok = await PTok.methods
      .balanceOf(accounts[2])
      .call({ from: accounts[2] });
    assert.strictEqual(consumerPTok, "300");
  });
});
