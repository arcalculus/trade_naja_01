const assert = require("assert");
const ganache = require("ganache-cli");

// use ganache virtual node instead of geth
const Web3 = require("web3");
//const web3 = new Web3(ganache.provider());
const web3 = new Web3("http://127.0.0.1:7545");

// compiled contracts
const participant = require("../ethereum/build/Participant.json");
const mcoin = require("../ethereum/build/MCoin.json");
const ptok = require("../ethereum/build/PTok.json");
const market = require("../ethereum/build/EnergyMarket.json");

let Participant;
let MCoin;
let PTok;
let Market;
let accounts;

beforeEach(async () => {
  // get all accounts
  accounts = await web3.eth.getAccounts();

  // account details
  // accounts[0]  -->  MEA
  // accounts[1]  -->  Prosumer "Latae Solar Energy"
  // accounts[2]  -->  Prosumer "Latae Cosmic Energy"
  // accounts[3]  -->  Prosumer "Latae Holy Energy"
  // accounts[4]  -->  Consumer "Americano Consume"

  // deploy contracts & set up contract references
  Participant = await new web3.eth.Contract(JSON.parse(participant.interface))
    .deploy({
      data: participant.bytecode
    })
    .send({
      from: accounts[0],
      gas: "1000000"
    });

  MCoin = await new web3.eth.Contract(JSON.parse(mcoin.interface))
    .deploy({
      data: mcoin.bytecode,
      arguments: ["1000000"]
    })
    .send({
      from: accounts[0],
      gas: "1000000"
    });

  PTok = await new web3.eth.Contract(JSON.parse(ptok.interface))
    .deploy({ data: ptok.bytecode })
    .send({ from: accounts[0], gas: "1000000" });

  const arguments = [
    Participant.options.address,
    MCoin.options.address,
    PTok.options.address,
    "10",
    "3",
    "1"
  ];

  Market = await new web3.eth.Contract(JSON.parse(market.interface))
    .deploy({
      data: market.bytecode,
      arguments: arguments
    })
    .send({
      from: accounts[0],
      gas: "3000000"
    });
});

describe("Energy Market Contract", () => {
  it("can deploy the EnergyMarket contract", async () => {
    Participant = await new web3.eth.Contract(JSON.parse(participant.interface))
      .deploy({
        data: participant.bytecode
      })
      .send({
        from: accounts[0],
        gas: "1000000"
      });
    assert.ok(Participant.options.address);

    MCoin = await new web3.eth.Contract(JSON.parse(mcoin.interface))
      .deploy({
        data: mcoin.bytecode,
        arguments: ["1000000"]
      })
      .send({
        from: accounts[0],
        gas: "1000000"
      });
    assert.ok(MCoin.options.address);

    PTok = await new web3.eth.Contract(JSON.parse(ptok.interface))
      .deploy({ data: ptok.bytecode })
      .send({ from: accounts[0], gas: "1000000" });
    assert.ok(PTok.options.address);

    const arguments = [
      Participant.options.address,
      MCoin.options.address,
      PTok.options.address,
      "10",
      "3",
      "1"
    ];

    Market = await new web3.eth.Contract(JSON.parse(market.interface))
      .deploy({
        data: market.bytecode,
        arguments: arguments
      })
      .send({
        from: accounts[0],
        gas: "3000000"
      });
    assert.ok(Market.options.address);
  });

  it("allows registered prosumers to sell energy and transition the energy block to 'Available' state", async () => {
    // accounts[1] registers as a prosumer
    await Participant.methods
      .enlistProsumer("Latae Solar Energy", "120000", "5")
      .send({
        from: accounts[1],
        gas: "1000000"
      });

    await Market.methods.sellEnergy("10", "3", "120", "2").send({
      from: accounts[1],
      gas: "1000000"
    });

    const energyBlockCount = await Market.methods.getEnergyBlockCount().call({
      from: accounts[1],
      gas: "1000000"
    });

    assert.strictEqual(energyBlockCount, "1");
  });

  it("allows registered consumers to buy energy and transition the energy block to 'Reserved' state", async () => {
    // prosumer registers and offers energy for sale
    await Participant.methods
      .enlistProsumer("Latae Solar Energy", "120000", "5")
      .send({
        from: accounts[1],
        gas: "1000000"
      });

    await Market.methods.sellEnergy("10", "3", "120", "2").send({
      from: accounts[1],
      gas: "1000000"
    });

    // consumer registers and buy an energy block
    await Participant.methods.enlistConsumer("Americano Consume").send({
      from: accounts[4],
      gas: "1000000"
    });

    // transfer some MCoins to consumers
    await MCoin.methods
      .transferFrom(accounts[0], accounts[4], "100000")
      .send({ from: accounts[0] });

    await Market.methods.buyEnergy("0").send({
      from: accounts[4],
      gas: "1000000"
    });

    // get details on the energy block
    const block = await Market.methods.energyBlocks("0").call({
      from: accounts[4],
      gas: "1000000"
    });

    // check if energy block details are correct
    assert.strictEqual(block.prosumer, accounts[1]);
    assert.strictEqual(block.consumer, accounts[4]);
    assert.strictEqual(block.tStart, "10");
    assert.strictEqual(block.tPeriod, "3");
    assert.strictEqual(block.target, "120");
    assert.strictEqual(block.unitPrice, "2");
    assert.strictEqual(block.actualConsumed, "0");
    assert.strictEqual(block.actualGenerated, "0");
    assert.strictEqual(block.deposited, "720");
    assert.strictEqual(block.pendingPayment, "0");
    assert.strictEqual(block.status, "0");
    assert.strictEqual(block.state, "1");

    // check MCoin balance of the consumer
    // MCoin should be 100,000 - 720 = 99,280
    const mcoinBalance = await MCoin.methods
      .balanceOf(accounts[4])
      .call({ from: accounts[4] });
    assert.strictEqual(mcoinBalance, "99280");

    // check PTok balance of the consumer
    const ptokBalance = await PTok.methods
      .balanceOf(accounts[4])
      .call({ from: accounts[4] });
    assert.strictEqual(ptokBalance, "360");
  });

  it("allows energy blocks to transition from 'Reserved' to 'Exchanging Energy' state when the time comes", async () => {
    // prosumer registers and offers energy for sale
    await Participant.methods
      .enlistProsumer("Latae Solar Energy", "120000", "5")
      .send({
        from: accounts[1],
        gas: "1000000"
      });

    await Market.methods.sellEnergy("10", "3", "120", "2").send({
      from: accounts[1],
      gas: "1000000"
    });

    // consumer registers and buy an energy block
    await Participant.methods.enlistConsumer("Americano Consume").send({
      from: accounts[4],
      gas: "1000000"
    });

    // transfer some MCoins to consumers
    await MCoin.methods
      .transferFrom(accounts[0], accounts[4], "100000")
      .send({ from: accounts[0] });

    await Market.methods.buyEnergy("0").send({
      from: accounts[4],
      gas: "1000000"
    });

    // get details on the energy block
    let block = await Market.methods.energyBlocks("0").call({
      from: accounts[4],
      gas: "1000000"
    });

    // the state should be "Reserved"
    assert.strictEqual(block.state, "1");

    // transition to "Exchanging" state
    await Market.methods.startExchanging("0").send({
      from: accounts[1],
      gas: "1000000"
    });

    // update block
    block = await Market.methods.energyBlocks("0").call({
      from: accounts[4],
      gas: "1000000"
    });

    // the state should be "Exchanging"
    assert.strictEqual(block.state, "2");
  });

  it("allows meter readings and transition the energy block from 'Exchanging Energy' to 'ReadyToSettle' state when the contract is over", async () => {
    // prosumer registers and offers energy for sale
    await Participant.methods
      .enlistProsumer("Latae Solar Energy", "120000", "5")
      .send({
        from: accounts[1],
        gas: "1000000"
      });

    await Market.methods.sellEnergy("10", "3", "120", "2").send({
      from: accounts[1],
      gas: "1000000"
    });

    // consumer registers and buy an energy block
    await Participant.methods.enlistConsumer("Americano Consume").send({
      from: accounts[4],
      gas: "1000000"
    });

    // transfer some MCoins to consumers
    await MCoin.methods
      .transferFrom(accounts[0], accounts[4], "100000")
      .send({ from: accounts[0] });

    await Market.methods.buyEnergy("0").send({
      from: accounts[4],
      gas: "1000000"
    });

    // get details on the energy block
    let block = await Market.methods.energyBlocks("0").call({
      from: accounts[4],
      gas: "1000000"
    });

    // the state should be "Reserved"
    assert.strictEqual(block.state, "1");

    // transition to "Exchanging" state
    await Market.methods.startExchanging("0").send({
      from: accounts[1],
      gas: "1000000"
    });

    // prosumer meter reports readings
    await Market.methods
      .reportGenerated("0", "360")
      .send({ from: accounts[1], gas: "1000000" });

    // consumer meter reports readings
    await Market.methods
      .reportConsumed("0", "360")
      .send({ from: accounts[4], gas: "1000000" });

    // update block
    block = await Market.methods.energyBlocks("0").call({
      from: accounts[4],
      gas: "1000000"
    });
    assert.strictEqual(block.state, "3");
    assert.strictEqual(block.actualConsumed, "360");
    assert.strictEqual(block.actualGenerated, "360");

    // PTok balance of the consumer should be exactly 0
    const ptokBalance = await PTok.methods
      .balanceOf(accounts[4])
      .call({ from: accounts[4], gas: "1000000" });

    assert.strictEqual(ptokBalance, "0");
  });

  it("(=) can settle energy consumption when consumer consumes exactly the amount contracted and transition the energy block to 'Settled' state", async () => {
    // prosumer registers and offers energy for sale
    await Participant.methods
      .enlistProsumer("Latae Solar Energy", "120000", "5")
      .send({
        from: accounts[1],
        gas: "1000000"
      });

    await Market.methods.sellEnergy("10", "3", "120", "2").send({
      from: accounts[1],
      gas: "1000000"
    });

    // consumer registers and buy an energy block
    await Participant.methods.enlistConsumer("Americano Consume").send({
      from: accounts[4],
      gas: "1000000"
    });

    // transfer some MCoins to consumers
    await MCoin.methods
      .transferFrom(accounts[0], accounts[4], "100000")
      .send({ from: accounts[0] });

    await Market.methods.buyEnergy("0").send({
      from: accounts[4],
      gas: "1000000"
    });

    // get details on the energy block
    let block = await Market.methods.energyBlocks("0").call({
      from: accounts[4],
      gas: "1000000"
    });

    // the state should be "Reserved"
    assert.strictEqual(block.state, "1");

    // transition to "Exchanging" state
    await Market.methods.startExchanging("0").send({
      from: accounts[1],
      gas: "1000000"
    });

    // prosumer meter reports readings
    await Market.methods
      .reportGenerated("0", "360")
      .send({ from: accounts[1], gas: "1000000" });

    // consumer meter reports readings & trainsition to "ReadyToSettle" state
    await Market.methods
      .reportConsumed("0", "360")
      .send({ from: accounts[4], gas: "1000000" });

    // market owner calls settle periodically I think
    await Market.methods.settle("0").send({
      from: accounts[0],
      gas: "5000000"
    });

    // check balance if things have been settled
    block = await Market.methods.energyBlocks("0").call({
      from: accounts[1],
      gas: "1000000"
    });

    // state should be Settled
    assert.strictEqual(block.state, "5");

    // deposit should be zero
    assert.strictEqual(block.deposited, "0");

    // status should be onTarget
    assert.strictEqual(block.status, "0");

    // MCoin of MEA accounts[0] should be 900,000 (original) + 72 (service fees)
    const meaMCoin = await MCoin.methods.balanceOf(accounts[0]).call({
      from: accounts[0],
      gas: "1000000"
    });
    assert.strictEqual(meaMCoin, "900072");

    // MCoin of prosumer accounts[1] should be 720 - 72 = 648
    const proMCoin = await MCoin.methods.balanceOf(accounts[1]).call({
      from: accounts[1],
      gas: "1000000"
    });
    assert.strictEqual(proMCoin, "648");

    // MCoin of consumer accounts[4] should be 100,000 - 720 = 99,280
    const conMCoin = await MCoin.methods.balanceOf(accounts[4]).call({
      from: accounts[4],
      gas: "1000000"
    });
    assert.strictEqual(conMCoin, "99280");

    // PTok of consumer accounts[4] should be exactly 0
    const conPTok = await PTok.methods.balanceOf(accounts[4]).call({
      from: accounts[4],
      gas: "1000000"
    });
    assert.strictEqual(conPTok, "0");
  });

  it("(<) can settle energy consumption when consumer consumes less than amount contracted and transition the energy block to 'Settled' state", async () => {
    // prosumer registers and offers energy for sale
    await Participant.methods
      .enlistProsumer("Latae Solar Energy", "120000", "5")
      .send({
        from: accounts[1],
        gas: "1000000"
      });

    await Market.methods.sellEnergy("10", "3", "120", "2").send({
      from: accounts[1],
      gas: "1000000"
    });

    // consumer registers and buy an energy block
    await Participant.methods.enlistConsumer("Americano Consume").send({
      from: accounts[4],
      gas: "1000000"
    });

    // transfer some MCoins to consumers
    await MCoin.methods
      .transferFrom(accounts[0], accounts[4], "100000")
      .send({ from: accounts[0] });

    await Market.methods.buyEnergy("0").send({
      from: accounts[4],
      gas: "1000000"
    });

    // get details on the energy block
    let block = await Market.methods.energyBlocks("0").call({
      from: accounts[4],
      gas: "1000000"
    });

    // the state should be "Reserved"
    assert.strictEqual(block.state, "1");

    // transition to "Exchanging" state
    await Market.methods.startExchanging("0").send({
      from: accounts[1],
      gas: "1000000"
    });

    // prosumer meter reports readings
    await Market.methods
      .reportGenerated("0", "360")
      .send({ from: accounts[1], gas: "1000000" });

    // consumer meter reports readings & trainsition to "ReadyToSettle" state
    await Market.methods
      .reportConsumed("0", "260")
      .send({ from: accounts[4], gas: "1000000" });

    // market owner calls settle periodically I think
    await Market.methods.settle("0").send({
      from: accounts[0],
      gas: "5000000"
    });

    // update block
    block = await Market.methods.energyBlocks("0").call({
      from: accounts[0],
      gas: "1000000"
    });

    // status should be LowerThanTarger(1)
    assert.strictEqual(block.status, "1");

    // deposit should be zero
    assert.strictEqual(block.deposited, "0");

    // state should be Settled(5)
    assert.strictEqual(block.state, "5");

    // MCoin of prosumer accounts[1] should be 720 - 72 = 648
    const proMCoin = await MCoin.methods.balanceOf(accounts[1]).call({
      from: accounts[1],
      gas: "1000000"
    });
    assert.strictEqual(proMCoin, "648");

    // MCoin of consumer account[4] should be 100,000 (original) - 720 (prepay for PTok) + 100 (sell back to MEA)
    const conMCoin = await MCoin.methods.balanceOf(accounts[4]).call({
      from: accounts[4],
      gas: "1000000"
    });
    assert.strictEqual(conMCoin, "99380");

    // MCoin of MEA accounts[0] should be 900,000 (original) + 72 (service fees) - 100 (buy back)
    const meaMCoin = await MCoin.methods.balanceOf(accounts[0]).call({
      from: accounts[0],
      gas: "1000000"
    });
    assert.strictEqual(meaMCoin, "899972");

    // PTok of consumer should reduce to exactly 0
    // PTok of consumer accounts[4] should be exactly 0
    const conPTok = await PTok.methods.balanceOf(accounts[4]).call({
      from: accounts[4],
      gas: "1000000"
    });
    assert.strictEqual(conPTok, "0");
  });

  /*   it("(>) can settle energy consumption when consumer consumes more than amount contracted and transition the energy block to 'Settled' state", async () => {
    assert(false);
  });

  it("(<<) can settle energy consumption when prosumer generates less than amount contracted and transition the energy block to 'Settled' state", async () => {
    assert(false);
  }); */
});
