const assert = require("assert");
const ganache = require("ganache-cli");

// use ganache virtual node instead of geth
const Web3 = require("web3");
//const web3 = new Web3(ganache.provider());
const web3 = new Web3("http://127.0.0.1:7545");

const { interface, bytecode } = require("../ethereum/build/MCoin.json");

let MCoin;
let accounts;

beforeEach(async () => {
  // get all accounts
  accounts = await web3.eth.getAccounts();

  // deploy contract for testing
  MCoin = await new web3.eth.Contract(JSON.parse(interface))
    .deploy({
      data: bytecode,
      arguments: ["1000000"]
    })
    .send({
      from: accounts[0],
      gas: "1000000"
    });
});

describe("MCoin Contract", () => {
  it("can deploy the MCoin contract", async () => {
    MCoin = await new web3.eth.Contract(JSON.parse(interface))
      .deploy({
        data: bytecode,
        arguments: ["1000000"]
      })
      .send({
        from: accounts[0],
        gas: "1000000"
      });

    assert.ok(MCoin.options.address);
  });

  it("gives the contract creator the initial supply of MCoins", async () => {
    MCoin = await new web3.eth.Contract(JSON.parse(interface))
      .deploy({
        data: bytecode,
        arguments: ["1000000"]
      })
      .send({
        from: accounts[0],
        gas: "1000000"
      });

    const balance = await MCoin.methods.balanceOf(accounts[0]).call({
      from: accounts[0]
    });

    assert.strictEqual(balance, "1000000");
  });

  it("can transfer MCoin from one account to another", async () => {
    MCoin = await new web3.eth.Contract(JSON.parse(interface))
      .deploy({
        data: bytecode,
        arguments: ["1000000"]
      })
      .send({
        from: accounts[0],
        gas: "1000000"
      });
    // accounts[1] should have no Mcoins
    let balanceB = await MCoin.methods.balanceOf(accounts[1]).call({
      from: accounts[1]
    });
    assert.strictEqual(balanceB, "0");

    // transfer MCoin from accounts[0] to accounts[1]
    await MCoin.methods
      .transferFrom(accounts[0], accounts[1], "500000")
      .send({ from: accounts[0] });

    // accounts[0] should have 500,000 MCoins left
    let balanceA = await MCoin.methods.balanceOf(accounts[0]).call({
      from: accounts[0]
    });
    assert.strictEqual(balanceA, "500000");

    // accounts[1] should have 500,000 MCoins
    balanceB = await MCoin.methods.balanceOf(accounts[1]).call({
      from: accounts[1]
    });
    assert.strictEqual(balanceB, "500000");
  });
});
