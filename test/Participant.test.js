const assert = require("assert");
const ganache = require("ganache-cli");

// use ganache virtual node instead of geth
const Web3 = require("web3");
//const web3 = new Web3(ganache.provider());
const web3 = new Web3("http://127.0.0.1:7545");

const { interface, bytecode } = require("../ethereum/build/Participant.json");

let Participant;
let accounts;

beforeEach(async () => {
  // get all accounts
  accounts = await web3.eth.getAccounts();

  // deploy contract for testing
  Participant = await new web3.eth.Contract(JSON.parse(interface))
    .deploy({
      data: bytecode
    })
    .send({
      from: accounts[0],
      gas: "1000000"
    });

  assert.ok(Participant.options.address);
});

describe("Participant Contract", () => {
  it("can deploy the Participant contract", async () => {
    const result = await new web3.eth.Contract(JSON.parse(interface))
      .deploy({
        data: bytecode
      })
      .send({ from: accounts[0], gas: "1000000" });
    assert.ok(result.options.address);
  });

  it("can register a new prosumer", async () => {
    await Participant.methods
      .enlistProsumer("Latae Cosmic Energy", "120000", "5")
      .send({
        from: accounts[1],
        gas: "1000000"
      });

    const prosumerAddr = await Participant.methods.prosumerAddrs(0).call({
      from: accounts[1]
    });

    const prosumerDetails = await Participant.methods
      .getProsumerDetails(prosumerAddr)
      .call({
        from: accounts[0]
      });

    assert.strictEqual(prosumerDetails[0], "Latae Cosmic Energy");
    assert.strictEqual(prosumerDetails[1], "120000");
    assert.strictEqual(prosumerDetails[2], "5");
  });

  it("can register a new consumer", async () => {
    await Participant.methods.enlistConsumer("Latae Consume Inc.").send({
      from: accounts[2],
      gas: "1000000"
    });

    const consumerAddr = await Participant.methods.consumerAddrs(0).call({
      from: accounts[2]
    });

    const consumerDetails = await Participant.methods
      .getConsumerDetails(consumerAddr)
      .call({
        from: accounts[2]
      });

    assert.strictEqual(consumerDetails, "Latae Consume Inc.");
  });

  it("doesn't accept already registered prosumers", async () => {
    // add a new prosumer
    await Participant.methods
      .enlistProsumer("Latae Cosmic Energy", "120000", "5")
      .send({
        from: accounts[1],
        gas: "1000000"
      });

    // try to register again with the same address
    try {
      await Participant.methods
        .enlistProsumer("Latae Cosmic Energy", "120000", "5")
        .send({
          from: accounts[1],
          gas: "1000000"
        });
      assert(false);
    } catch (err) {
      assert(err);
    }
  });

  it("doesn't accept already registered consumers", async () => {
    // add a new consumer
    await Participant.methods.enlistConsumer("Latae Consume Inc.").send({
      from: accounts[2],
      gas: "1000000"
    });

    // try to register again with the same address
    try {
      await Participant.methods.enlistConsumer("Latae Consume Inc.").send({
        from: accounts[2],
        gas: "1000000"
      });
      assert(false);
    } catch (err) {
      assert(err);
    }
  });
});
