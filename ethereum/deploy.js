const fs = require("fs-extra");
const path = require("path");
const ganache = require("ganache-cli");
const Web3 = require("web3");

// use ganache virtual node to play with blockchain
//const web3 = new Web3(ganache.provider());
const web3 = new Web3("http://127.0.0.1:7545");

const mcoin = require("./build/MCoin.json");
const ptok = require("./build/PTok.json");
const participant = require("./build/Participant.json");
const emarket = require("./build/EnergyMarket.json");

let contracts = {
  mcoin: {
    interface: mcoin.interface,
    bytecode: mcoin.bytecode,
    address: ""
  },
  ptok: {
    interface: ptok.interface,
    bytecode: ptok.bytecode,
    address: ""
  },
  participant: {
    interface: participant.interface,
    bytecode: participant.bytecode,
    address: ""
  },
  emarket: {
    interface: emarket.interface,
    bytecode: emarket.bytecode,
    address: ""
  }
};

const deploy = async (
  interface,
  bytecode,
  contractName,
  from,
  arguments = []
) => {
  const contractNameCap =
    contractName.charAt(0).toUpperCase() + contractName.slice(1);
  //console.log(`Deploying ${contractNameCap} from ${from}`);
  try {
    const result = await new web3.eth.Contract(JSON.parse(interface))
      .deploy({
        data: `0x${bytecode}`,
        arguments: arguments
      })
      .send({
        from: from,
        gas: "3000000"
      });
    //console.log(`${contractNameCap} deployed to ${result.options.address}`);
    console.log(`const ${contractName}Addr = "${result.options.address}";`);
    return result.options.address;
  } catch (err) {
    console.log(`Error when deploying ${contractNameCap}: ${err.message}`);
  }
};

(async () => {
  const accounts = await web3.eth.getAccounts();
  await web3.eth.personal.unlockAccount(accounts[0], "");
  // Deploy MCoin
  contracts.mcoin.address = await deploy(
    contracts.mcoin.interface,
    contracts.mcoin.bytecode,
    "mcoin",
    accounts[0],
    ["1000000"]
  );

  // Deploy PTok
  contracts.ptok.address = await deploy(
    contracts.ptok.interface,
    contracts.ptok.bytecode,
    "ptok",
    accounts[0]
  );

  // Deploy Participant
  contracts.participant.address = await deploy(
    contracts.participant.interface,
    contracts.participant.bytecode,
    "participant",
    accounts[0]
  );

  // Deploy EnergyMarket
  contracts.emarket.address = await deploy(
    contracts.emarket.interface,
    contracts.emarket.bytecode,
    "emarket",
    accounts[0],
    [
      contracts.participant.address,
      contracts.mcoin.address,
      contracts.ptok.address,
      "10",
      "3",
      "1"
    ]
  );
})();

module.exports.contracts = contracts;
