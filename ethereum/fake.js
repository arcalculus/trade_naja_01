const fs = require("fs-extra");
const path = require("path");
const Web3 = require("web3");
const logSymbols = require("log-symbols");

// use ganache virtual node to play with blockchain
//const web3 = new Web3(ganache.provider());
const web3 = new Web3("http://127.0.0.1:7545");

// fun stuff: creating bullshit
(async () => {
  /*--------------------------------- PREP ------------------------------------*/
  // contract ABI
  const mcoin = require("./build/MCoin.json");
  const ptok = require("./build/PTok.json");
  const participant = require("./build/Participant.json");
  const emarket = require("./build/EnergyMarket.json");

// contract address
const mcoinAddr = "0x57996a25A5ebF6Ee316727EE866921065413b641";
const ptokAddr = "0x156D9210cCdaDABA91Af492E3c191e162964cc5D";
const participantAddr = "0x7e4671a6EB340B24743Aaf450CFABeaC5eA40Af8";
const emarketAddr = "0x5adF6E2ec21CC6f755772aC3644A4fe6EB54D9E2";


  // Uncle Bear Ganache Addresses
  /*const mcoinAddr = "0x02b99704E77eA3421D6E17dBAfFDd7AA1061970d";
  const ptokAddr = "0x96340E1C406385B7070594D20aC9c444C415Db1c";
  const participantAddr = "0xE7a97E1C32fa5dC290F8E994173ef5C1b162b44B";
  const emarketAddr = "0x7CDDC186f2950618B620B9BB54448Ffe3D759809";*/

  // load contracts for creating fake data
  const Participant = new web3.eth.Contract(
    JSON.parse(participant.interface),
    participantAddr
  );

  const MCoin = new web3.eth.Contract(JSON.parse(mcoin.interface), mcoinAddr);

  const PTok = new web3.eth.Contract(JSON.parse(ptok.interface), ptokAddr);

  const Market = new web3.eth.Contract(
    JSON.parse(emarket.interface),
    emarketAddr
  );

  /*------------------------------ REAL WORK -----------------------------------*/
  console.log(
    logSymbols.info,
    "Creating bullshit data for managers and executives..."
  );

  // get a list of all accounts
  const accounts = await web3.eth.getAccounts();
  const mea = accounts[0];
  const lataeSolar = accounts[1];
  const lataeCosmic = accounts[2];
  const lataeWind = accounts[3];
  const lataeIceCream = accounts[4];
  const lataeCoffeeConsume = accounts[5];
  const lataeAmericanoConsume = accounts[6];
  const thamesConsume = accounts[7];

  // fun & useless stuff
  console.log(logSymbols.success, `Generated MEA god account at ${mea}`);
  console.log(
    logSymbols.success,
    `Generated Latae Solar prosumer account at ${lataeSolar}`
  );
  console.log(
    logSymbols.success,
    `Generated Latae Cosmic prosumer account at ${lataeCosmic}`
  );
  console.log(
    logSymbols.success,
    `Generated Latae Wind prosumer account at ${lataeWind}`
  );
  console.log(
    logSymbols.success,
    `Generated Latae Ice Cream prosumer account at ${lataeIceCream}`
  );
  console.log(
    logSymbols.success,
    `Generated Latae Coffee consumer account at ${lataeCoffeeConsume}`
  );
  console.log(
    logSymbols.success,
    `Generated Latae Americano consumer account at ${lataeAmericanoConsume}`
  );
  console.log(
    logSymbols.success,
    `Generated Latae Americano consumer account at ${lataeAmericanoConsume}`
  );
  console.log(
    logSymbols.success,
    `Generated Thames consumer account at ${thamesConsume}`
  );

  try {
    // MEA already deployed all the contracts so MEA will give thamesConsume some MCoin.
    console.log(logSymbols.info, "Setting up EnergyMarket");
    await MCoin.methods
      .transferFrom(mea, thamesConsume, "1000")
      .send({ from: mea });

    console.log(logSymbols.success, "MEA transfers 1,000 MCoins to Thames");

    await MCoin.methods
      .transferFrom(mea, lataeCoffeeConsume, "55000")
      .send({ from: mea });

    console.log(
      logSymbols.success,
      "MEA transfers 55,000 MCoins to Latae Coffee"
    );

    // Latae energy group wants to sell energy
    console.log(
      logSymbols.success,
      "Latae Solar registers as a prosumer on the trading platform"
    );
    await Participant.methods
      .enlistProsumer("Latae Solar Group", "120000", "3")
      .send({
        from: lataeSolar,
        gas: "1000000"
      });

    // generate some PToks
    console.log(logSymbols.success, "Latae Solar generates some PToks");
    await PTok.methods.generateEnergy(lataeSolar, "2000").send({
      from: lataeSolar,
      gas: "1000000"
    });

    console.log(
      logSymbols.success,
      "Latae Cosmic registers as a prosumer on the trading platform"
    );
    await Participant.methods
      .enlistProsumer("Latae Cosmic Group", "350000", "5")
      .send({
        from: lataeCosmic,
        gas: "1000000"
      });

    console.log(
      logSymbols.success,
      "Latae Wind registers as a prosumer on the trading platform"
    );
    await Participant.methods
      .enlistProsumer("Latae Wind Group", "50000", "1")
      .send({
        from: lataeWind,
        gas: "1000000"
      });

    console.log(
      logSymbols.success,
      "Latae Ice Cream registers as a prosumer on the trading platform"
    );
    await Participant.methods
      .enlistProsumer("Latae Ice Cream Group", "5000000", "30")
      .send({
        from: lataeIceCream,
        gas: "1000000"
      });

    console.log(
      logSymbols.success,
      "Thames registers as a consumer on the trading platform"
    );
    await Participant.methods.enlistConsumer("Thames the Consumer").send({
      from: thamesConsume,
      gas: "1000000"
    });

    console.log(
      logSymbols.success,
      "Latae Coffee Consume registers as a consumer on the trading platform"
    );
    await Participant.methods.enlistConsumer("Latae Coffee Consume").send({
      from: lataeCoffeeConsume,
      gas: "1000000"
    });

    console.log(
      logSymbols.success,
      "Latae Americano Consume registers as a consumer on the trading platform"
    );
    await Participant.methods.enlistConsumer("Latae Americano Consume").send({
      from: lataeAmericanoConsume,
      gas: "1000000"
    });

    /*console.log(logSymbols.info, "Populating EnergyMarket with energy offers");
    console.log(logSymbols.success, "Latae Solar group sells solar energy");
    await Market.methods
      .sellEnergy("10", "3", "120", "2")
      .send({ from: lataeSolar, gas: "1000000" });

    console.log(
      logSymbols.success,
      "Latae Cosmic group sells cosmic supernova-grade energy"
    );
    await Market.methods
      .sellEnergy("16", "1", "45000", "3")
      .send({ from: lataeCosmic, gas: "1000000" });

    console.log(logSymbols.success, "Latae Wind group sells wind energy");
    await Market.methods
      .sellEnergy("22", "2", "100", "1")
      .send({ from: lataeWind, gas: "1000000" });

    console.log(
      logSymbols.success,
      "Latae Ice Cream group sells nuclear energy"
    );
    await Market.methods
      .sellEnergy("12", "4", "350", "4")
      .send({ from: lataeIceCream, gas: "1000000" });

    console.log(logSymbols.info, "Creating initial transactions");
    console.log(
      logSymbols.success,
      "Thames the consumer bought an energy block from Latae Solar Group"
    );
    await Market.methods.buyEnergy("0").send({
      from: thamesConsume,
      gas: "1000000"
    });

    console.log(
      logSymbols.success,
      "Latae the Coffee consumer bought an energy block from Latae Ice Cream Group"
    );
    await Market.methods.buyEnergy("3").send({
      from: lataeCoffeeConsume,
      gas: "1000000"
    });

    console.log(logSymbols.info, "Triggering energy exchaning");
    console.log(
      logSymbols.success,
      "Automatically trigger the energy exchange bound by the contract"
    );
    await Market.methods.startExchanging("0").send({
      from: lataeSolar,
      gas: "1000000"
    });

    console.log(logSymbols.info, "Interfacing with smart meters");
    console.log(
      logSymbols.success,
      "Latae Solar Group's smart meter automatically sends its actual generation"
    );
    await Market.methods.reportGenerated("0", "360").send({
      from: lataeSolar,
      gas: "1000000"
    });

    console.log(
      logSymbols.info,
      "Thames' smart meter automatically sends its actual consumption"
    );
    await Market.methods.reportConsumed("0", "360").send({
      from: thamesConsume,
      gas: "1000000"
    });

    console.log(logSymbols.info, "Settilng energy exchange");
    console.log(
      logSymbols.success,
      "Automatically trigger energy settlement process"
    );
    await Market.methods.settle("0").send({
      from: mea,
      gas: "1000000"
    });*/
  } catch (err) {
    console.log("Can't create bullshit. Something's wrong: ", err.message);
  }
})();
