pragma solidity ^0.4.19;

/*******************************************************************************
 *                            ERC20TOKEN INTERFACE                             *
 ******************************************************************************/
interface ERC20Token {
    function balanceOf(address owner) external view returns(uint256 balance);
    function transfer(address to, uint tokens) external returns (bool success);
    function transferFrom(address from, address to, uint tokens) external returns (bool success);
    function approve(address spender, uint tokens) external returns (bool success);
    function allowance(address tokenOwner, address spender) external view returns (uint remaining);

    event Transfer(address indexed from, address indexed to, uint tokens);
    event Approval(address indexed tokenOwner, address indexed spender, uint tokens);
}

/*******************************************************************************
 *                               TEMPORARY MCOIN                               *
 ******************************************************************************/
contract MCoin is ERC20Token {
    string public name;
    string public symbol;
    uint8 public decimals = 18;
    uint256 public totalSupply;
  
    // balanceOf for each account
    mapping(address => uint256) balances;
    // Owner of account approves the transfer of an amount to another account
    mapping(address => mapping (address => uint256)) allowed;
    
    // constructor
    function MCoin(uint256 initialSupply) public {
        name = "MCoin";
        symbol = "MCoin";
        //totalSupply = initialSupply * 10 ** uint256(decimals);
        totalSupply = initialSupply;
        balances[msg.sender] = totalSupply; 
    }
    
    /**
     * Internal transfer, only can be called by this contract
     */
    function _transfer(address _from, address _to, uint _value) internal {
        // Check if the sender has enough
        require(balances[_from] >= _value);
        // Check for overflows
        require(balances[_to] + _value >= balances[_to]);
        // Subtract from the sender
        balances[_from] -= _value;
        // Add the same to the recipient
        balances[_to] += _value;
        Transfer(_from, _to, _value);
    }
    
    // get balance of a address
    function balanceOf(address owner) external view returns(uint256 balance) {
        return balances[owner];
    }
    
    // Transfer the balance from owner's account to another account
    function transfer(address to, uint tokens) external returns (bool success) {
        _transfer(msg.sender, to, tokens);
        return true;
    }
    
    // Send `tokens` amount of tokens from address `from` to address `to`
    // The transferFrom method is used for a withdraw workflow, allowing contracts to send
    // tokens on your behalf, for example to "deposit" to a contract address and/or to charge
    // fees in sub-currencies; the command should fail unless the _from account has
    // deliberately authorized the sender of the message via some mechanism; we propose
    // these standardized APIs for approval:
    function transferFrom(address from, address to, uint tokens) external returns (bool success) {
        //require(tokens <= allowed[from][msg.sender]);
        //allowed[from][msg.sender] -= tokens;
        _transfer(from, to, tokens);
        return true;
    }
 
    // Allow `spender` to withdraw from your account, multiple times, up to the `tokens` amount.
    // If this function is called again it overwrites the current allowance with _value.
    function approve(address spender, uint tokens) external returns (bool success) {
        allowed[msg.sender][spender] = tokens;
        Approval(msg.sender, spender, tokens);
        return true;
    }

    // Returns the amount of tokens approved by the owner that can be
    // transferred to the spender's account
    function allowance(address tokenOwner, address spender) external view returns (uint remaining) {
        return allowed[tokenOwner][spender];
    }  

}

/*******************************************************************************
 *                               TEMPORARY PTOK                                *
 ******************************************************************************/
contract PTok is ERC20Token {    
    string public name;
    string public symbol;
    uint8 public decimals = 18;
    uint256 public totalSupply;

    event Generated(
      address _customer, 
      uint256 _energyGenerated, 
      uint256 energyBalance, 
      uint256 _totalSupply
    );

    event Used(
      address _customer, 
      uint256 _energyConsume, 
      uint256 energyBalance, 
      uint256 _totalSupply
    );

    // balanceOf for each account
    mapping(address => uint256)  balances;
    // Owner of account approves the transfer of an amount to another account
    mapping(address => mapping (address => uint256)) allowed;
    
    // constructor
    function PTok() public {
        name = "Pream Tok"; // Set the name for display purposes
        symbol = "PTok"; // Set the symbol for display purposes
    }
    
    /**
     * Internal transfer, only can be called by this contract
     */
    function _transfer(address _from, address _to, uint _value) internal {
        // Check if the sender has enough
         require(balances[_from] >= _value);
        // Check for overflows
         require(balances[_to] + _value >= balances[_to]);
        // Subtract from the sender
        balances[_from] -= _value;
        // Add the same to the recipient
        balances[_to] += _value;
        Transfer(_from, _to, _value);
    }
    
    // get balance of a address
    function balanceOf(address owner) external view returns(uint256 balance) {
        return balances[owner];
    }
    
    // Transfer the balance from owner's account to another account
    function transfer(address to, uint tokens) external returns (bool success) {
        _transfer(msg.sender, to, tokens);
        return true;
    }
    
    // Send `tokens` amount of tokens from address `from` to address `to`
    // The transferFrom method is used for a withdraw workflow, allowing contracts to send
    // tokens on your behalf, for example to "deposit" to a contract address and/or to charge
    // fees in sub-currencies; the command should fail unless the _from account has
    // deliberately authorized the sender of the message via some mechanism; we propose
    // these standardized APIs for approval:
    function transferFrom(address from, address to, uint tokens) external returns (bool success) {
        //require(tokens <= allowed[from][msg.sender]);
        //allowed[from][msg.sender] -= tokens;
        _transfer(from, to, tokens);
        return true;
    }
 
    // Allow `spender` to withdraw from your account, multiple times, up to the `tokens` amount.
    // If this function is called again it overwrites the current allowance with _value.
    function approve(address spender, uint tokens) external returns (bool success) {
        allowed[msg.sender][spender] = tokens;
        Approval(msg.sender, spender, tokens);
        return true;
    }

    // Returns the amount of tokens approved by the owner that can be
    // transferred to the spender's account
    function allowance(address tokenOwner, address spender) external view returns (uint remaining) {
        return allowed[tokenOwner][spender];
    }  

    // Use this function to burn customers's energy (this also be used in EV Charging)
    function useEnergy(address _customer, uint256 _energy) public {
        uint256 energyInt = getActualEnergyUnitValue(_energy);
        balances[_customer] -= energyInt;
        totalSupply -= energyInt;
        // log event
        Used(_customer, energyInt, balances[_customer], totalSupply); 
    }

    // Use this function to generate your own energy to use or sell
    function generateEnergy(address _genAddr, uint256 _energy) public {
        uint256 energyInt = getActualEnergyUnitValue(_energy);
        balances[_genAddr] += energyInt;
        totalSupply += energyInt;        
        // log event
        Generated(_genAddr, energyInt, balances[_genAddr], totalSupply);
    }

    function getActualEnergyUnitValue(uint256 _energy) public view returns (uint256 value) {
        //return _energy * 10 ** uint256(decimals);
        return _energy;
    }
}

/*******************************************************************************
 *                                   PARTICIPANT                               *
 ******************************************************************************/
 
contract Participant {
    
    address public owner;
    
    struct ProsumerDetails {
        string name;
        uint generatingCapacity;
        uint reliabilityRatings;
    }
    
    address[] public prosumerAddrs;
    address[] public consumerAddrs;
    mapping(address => bool) public prosumerRegistry;
    mapping(address => bool) public consumerRegistry;
    mapping(address => ProsumerDetails) public prosumers;
    mapping(address => ConsumerDetails) public consumers;

    struct ConsumerDetails {
        string name;
    }
    
    event ProsumerRegistered(string _name, uint generatingCapacity, uint reliabilityRatings);
    event ConsumerRegistered(string name);
    
    function Participant() public  {
        owner = msg.sender;
    }
    
    function enlistProsumer(string _name, uint _generatingCapacity, uint _reliabilityRatings) public returns (bool) {
        require(!prosumerRegistry[msg.sender]);
        prosumerAddrs.push(msg.sender);
        ProsumerDetails memory newProsumer = ProsumerDetails({
            name: _name,
            generatingCapacity: _generatingCapacity,
            reliabilityRatings: _reliabilityRatings
        });
        
        prosumers[msg.sender] = newProsumer;
        prosumerRegistry[msg.sender] = true;
        
        ProsumerRegistered(newProsumer.name, newProsumer.generatingCapacity, newProsumer.reliabilityRatings);
        
        return true;
    }
    
    function enlistConsumer(string _name) public returns (bool) {
        require(!consumerRegistry[msg.sender]);
        consumerAddrs.push(msg.sender);
        ConsumerDetails memory newConsumer = ConsumerDetails({
           name: _name 
        });
        
        consumers[msg.sender] = newConsumer;
        consumerRegistry[msg.sender] = true;
        
        ConsumerRegistered(newConsumer.name);
        
        return true;
    }
    
    function getProsumerAddresses() public view returns (address[]) {
        return prosumerAddrs;
    }
    
    function getConsumerAddresses() public view returns (address[]) {
        return consumerAddrs;
    }
    
    function getProsumerDetails(address _addr) public view returns (string, uint, uint) {
        return ( prosumers[_addr].name, prosumers[_addr].generatingCapacity, prosumers[_addr].reliabilityRatings );
    }

    function getConsumerDetails(address _addr) public view returns (string) {
        return consumers[_addr].name;
    }
}

contract EnergyMarket {
    address public owner;
    uint public platformFees;       // service charges for using the platform
    uint public defaultEnergyPrice; // default energy price
    uint public buyBackEnergyPrice; // default buy back price
    
    // status to handle shit when it happens
    // TODO: make use of LowerThanTarget and HigherThanTarget seriously
    enum Status { 
        onTarget,
        LowerThanTarget,
        HigherThanTarget
    }
        
    // ugly state machine
    // TODO: maybe I can use block.timestamp and make these timed transitions...
    enum State {
        Available, 
        Reserved,
        Exchanging,
        ReadyToSettle,
        WaitingForPayment,
        Settled
    }
        
    struct EnergyBlock {
        address prosumer;      // address of prosumer
        address consumer;      // address of consumer
        uint tStart;           // start time in epoch
        uint tPeriod;          // exchanging period in hours
        uint target;           // target kWh to generate & consume
        uint unitPrice;        // energy price in MCoin
        uint actualConsumed;   // kWh actually consumed
        uint actualGenerated;  // kWh actuallMetro Gridy generated
        uint deposited;        // keep track Metro Gridof how much the consumer pays MCoin
        uint pendingPayment;   // keep track Metro Gridof how much the consumer will need to pay if consumed > generated in MCoin
        Status status;         // status of tMetro Gridhis energy block
        State state;           // state of thMetro Gridis energy block
    }

    // all energy blocks in market new & oldMetro Grid
    EnergyBlock[] public energyBlocks;
    
    // references to another contracts, datastores, tokens and shit
    Participant private p;
    MCoin private mcoin;
    PTok private ptok;
    
    // events
    event NewEnergyOnSale(address _prosumer, uint _tStart, uint _tPeriod, uint _target, uint _unitPrice);
    event EnergyReserved(address _consumer, uint _index);
    event EnergyExchanging(address _from, address _to, uint _index);
    event ActualGenerationReported(uint _amount, uint _index);
    event ActualConsumptionReported(uint _amount, uint _index);
    event PaymentPending(uint _amount, uint _index);
    event EnergySettled(uint _index);
    event NewRegulationsEnforced(uint _platformFees, uint _defaultEnergyPrice, uint _buyBackEnergyPrice);
    
    // function modifiers
    modifier registeredProsumerOnly() {
        require(p.prosumerRegistry(msg.sender));
        _;
    }
    
    modifier registeredUserOnly() {
        require(p.prosumerRegistry(msg.sender) || p.consumerRegistry(msg.sender));
        _;
    }
    
    modifier ownerOnly() {
        require(owner == msg.sender);
        _;
    }
    
    // constructor
    function EnergyMarket(
        address _participantContractAddr, 
        address _mCoinAddr,
        address _pTokAddr,
        uint _platformFees, 
        uint _defaultEnergyPrice, 
        uint _buyBackEnergyPrice
    ) public {
        owner = msg.sender;
        p = Participant(_participantContractAddr);
        mcoin = MCoin(_mCoinAddr);
        ptok = PTok(_pTokAddr);
        
        // service charges
        platformFees = _platformFees;
        defaultEnergyPrice = _defaultEnergyPrice;
        buyBackEnergyPrice = _buyBackEnergyPrice;
    }
    
    function sellEnergy(
        uint _tStart, 
        uint _tPeriod, 
        uint _target, 
        uint _unitPrice
    ) public registeredProsumerOnly returns (uint) {
        
        EnergyBlock memory newBlock = EnergyBlock({
            prosumer: msg.sender,
            consumer: msg.sender,
            tStart: _tStart,
            tPeriod: _tPeriod,
            target: _target,
            unitPrice: _unitPrice,
            actualGenerated: 0,
            actualConsumed: 0,
            deposited: 0,
            pendingPayment: 0,
            status: Status.onTarget,
            state: State.Available
        });
        
        // generate PToks but MEA won't transfer MCoin atm
        uint totalPTok = newBlock.target * newBlock.tPeriod;
        ptok.generateEnergy(msg.sender, totalPTok);
        ptok.transferFrom(msg.sender, this, totalPTok);
        
        energyBlocks.push(newBlock);
        NewEnergyOnSale(msg.sender, _tStart, _tPeriod, _target, _unitPrice);
        return (energyBlocks.length - 1);
    }
    
    function buyEnergy(uint _index) public payable registeredUserOnly {
        EnergyBlock storage e = energyBlocks[_index];
        uint totalPTok = e.tPeriod * e.target;
        uint totalMCoin = e.unitPrice * totalPTok;
        require(e.state == State.Available);
        
        e.state = State.Reserved;
        e.consumer = msg.sender;
        
        mcoin.transferFrom(msg.sender, this, totalMCoin);
        ptok.transferFrom(this, msg.sender, totalPTok);
        e.deposited += totalMCoin; 

        EnergyReserved(msg.sender, _index);
    }
    
    function startExchanging(uint _index) public payable registeredProsumerOnly {
        EnergyBlock storage e = energyBlocks[_index];
        require(e.state == State.Reserved);
        require(e.prosumer == msg.sender);
        e.state = State.Exchanging;

        EnergyExchanging(e.prosumer, e.consumer, _index);
    }
    
    // smart meters on prosumers can call this function, maybe 
    function reportGenerated(
        uint _index, 
        uint _actualGenerated
    ) public registeredProsumerOnly {
        EnergyBlock storage e = energyBlocks[_index];
        require(e.state == State.Exchanging);
        e.actualGenerated = _actualGenerated;
        
        // TODO: maybe I can compare this periodically (in front-end) and adjust
        // e.status accordingly
        if (e.actualGenerated > 0 && e.actualConsumed > 0) {
            e.state = State.ReadyToSettle;
        }

        ActualGenerationReported(_actualGenerated, _index);
    }
    
    // smart meters on consumers can call this function, maybe 
    function reportConsumed(
        uint _index, 
        uint _actualConsumed
    ) public registeredUserOnly {
        EnergyBlock storage e = energyBlocks[_index];
        require(e.state == State.Exchanging);
        require(e.consumer == msg.sender);

        e.actualConsumed = _actualConsumed;
        ptok.useEnergy(msg.sender, _actualConsumed);

        // TODO: maybe I can compare this periodically (in front-end) and adjust
        // e.status accordingly
        if (e.actualGenerated > 0 && e.actualConsumed > 0) {
            e.state = State.ReadyToSettle;
        }

        ActualConsumptionReported(_index, _actualConsumed);
    }
    
    // Settlement period
    function settle(uint _index) public {
        EnergyBlock storage e = energyBlocks[_index];
        require(e.state == State.ReadyToSettle);
        
        // the default charges for MEA
        uint ownerServiceCharge = e.deposited / platformFees;
        mcoin.transferFrom(this, owner, ownerServiceCharge);
        e.deposited -= ownerServiceCharge;
        
        // settle energy usage
        if (e.actualConsumed > e.actualGenerated) {
            e.status = Status.HigherThanTarget;

            // transfer the rest of deposit to prosumer
            mcoin.transferFrom(this, e.prosumer, e.deposited);
            e.deposited = 0;
            
            // consumer needs to buy higher price energy from MEA
            uint diff = e.actualGenerated - e.actualConsumed;
            e.pendingPayment = diff * defaultEnergyPrice;

            // wait for payment before closing transactions
            e.state = State.WaitingForPayment;

            PaymentPending(e.pendingPayment, _index);
        } else if (e.actualConsumed < e.actualGenerated) {
            e.status = Status.LowerThanTarget;

            // transfer the rest of deposit to prosumer
            mcoin.transferFrom(this, e.prosumer, e.deposited);
            e.deposited = 0;

            // transfer MCoins back to consumers at a lower price
            uint ptokLeft = ptok.balanceOf(e.consumer);
            uint buyBackAmount = ptokLeft * buyBackEnergyPrice;
            mcoin.transferFrom(owner, e.consumer, buyBackAmount);
            ptok.useEnergy(e.consumer, ptokLeft);

            // finish transaction
            e.state = State.Settled;
            EnergySettled(_index);
        } else {
            e.status = Status.onTarget;

            // trasnfer the rest of deposit to prosumer
            mcoin.transferFrom(this, e.prosumer, e.deposited);
            e.deposited = 0;

            // finish transaction
            e.state = State.Settled;
            EnergySettled(_index);
        }
    }
    
    // Set new regulations
    function enforceNewRegulations(
        uint _platformFees, 
        uint _defaultEnergyPrice, 
        uint _buyBackEnergyPrice
    ) public ownerOnly {
        platformFees = _platformFees;
        defaultEnergyPrice = _defaultEnergyPrice;
        buyBackEnergyPrice = _buyBackEnergyPrice;

        NewRegulationsEnforced(_platformFees, _defaultEnergyPrice, _buyBackEnergyPrice);
    }
    
    function getEnergyBlockCount() public view returns (uint) {
        return energyBlocks.length;
    }
    
    function deposit(uint _index, uint _amount) public registeredUserOnly payable {
        EnergyBlock storage e = energyBlocks[_index];
        // accept only extra payment
        require(e.state == State.WaitingForPayment);
        require(msg.value >= e.pendingPayment);
        
        // complete energy blocks
        e.state = State.Settled;
        EnergySettled(_index);
    }
}
