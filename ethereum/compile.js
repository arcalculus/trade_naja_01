const path = require("path");
const fs = require("fs-extra");
const solc = require("solc");

// remove build directory when re-compiling smart contracts
const buildPath = path.resolve(__dirname, "build");
fs.removeSync(buildPath);

// define smart contract location and compile the source with "solc" (solidity compiler)
const contractPath = path.resolve(__dirname, "contracts", "Combined.sol");
const source = fs.readFileSync(contractPath, "utf8");
const compiledContract = solc.compile(source, 1).contracts;

// create build directory to keep contract data (interface & bytecode)
fs.ensureDirSync(buildPath);

// You don't need a loop if you have only one contract in a file
Object.keys(compiledContract).forEach(name => {
  // save contract data in .json format for later use
  fs.outputJsonSync(
    path.resolve(buildPath, `${name.replace(":", "")}.json`),
    compiledContract[name]
  );
  console.log(`${name.replace(":", "")} compiled.`);
});
