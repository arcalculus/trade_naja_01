#Trade Naja Repository

## Setting up app
After you clone the repository, please run the following command to set up node modules. 
```
npm install
```

## Running app
We have some basic APIs to serve your client and you can start the service with:
```
npm run api
```
## Testing smart contracts
If you enjoy fixing smart contracts, you may find test suite useful. You may run the tests with:
```
npm run test
```

## Changing default provider
By default, trade_naja uses "ganache-cli" provider. If you want to use your own geth RPC provider, you can do this:
```javascript
const Web3 = require('web3');
const provider = new Web3.provider.HttpProvider('http://127.0.0.1:8545');
const web3 = new Web3(provider);
``` 

## Available APIs
### Ethereum Accounts
#### GET /accounts
List all ethereum accounts with balance
#### GET /accounts/:addr
List a single addr ethereum account detail with balance

### Registration (Smart Contract, Not MongoDB)
#### GET /prosumers
List all prosumers registered on the platform
#### GET /consumers
List all consumers registered on the platform
#### POST /prosumer
Register a new prosumer.

**Request parameters**:
```javascript
{
    "name": "AAA",
    "generatingCapacity": "120",
    "reliabilityRatings": "2"
}
```
#### POST /consumer
Register a new consumer

**Request parameters**:
```javascript
{
    "name": "AAA"
}
```
### Energy Market
### GET /energy/blocks
List all energy blocks on the market

### GET /energy/block/:id
Get details on the energy block number :id

### POST /energy/sell
Sell an energy block (only for registered prosumer)

**request parameters**
```javascript
{
    "tStart": "10",
    "tPeriod": "3",
    "target": "300",
    "unitPrice": "2"
}
```

### POST /energy/buy
Buy an energy block (only for registered consumer or prosumer)

**request parameters**
```javascript
{
    "energyId": "0"
}
```

### POST /energy/exchange
Start exchanging energy. Only manual trigger is implemented.
**request parameters**
```javascript
{
    "energyId": "0"
}
```

### POST /energy/consumption
Report actual consumption during energy exchange. It's assumed to be net.
Only the consumer who buys the energy block can call this API.
**request parameters**
```javascript
{
    "energyId": "0",
    "generation": "120"
}
```
### POST /energy/generation
Report actual consumption during energy exchange. It's assumed to be net.
Only the prosumer who sells the energy block can call this API.
**request parameters**
```javascript
{
    "energyId": "0",
    "generation": "120"
}
```

### POST /energy/settle
Settle the energy consumption and generation after exchanging period.
Only the Energy Market can call this API.
**request parameters**
```javascript
{
    "energyId": "0"
}
```


### GET /energy/events
Get all events that were emitted in the Energy Market. I've not filtered them. So response will be raw.

**Really sorry I don't have time to document all the responses to these requests because I have to sleep. I do hope that you can figure them out by calling the APIs and study them.**
