var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/";

MongoClient.connect(url, function(err, db) {
  if (err) throw err;
  var dbo = db.db("tradedb");
   
  dbo.createCollection("accounts", function(err, res) {
    if (err) throw err;
    console.log("Collection created!");
    db.close();
  });
     
  dbo.createCollection("prosumers", function(err, res) {
    if (err) throw err;
    console.log("Collection created!");
    db.close();
  });
      
  dbo.createCollection("consumers", function(err, res) {
    if (err) throw err;
    console.log("Collection created!");
    db.close();
  });
});