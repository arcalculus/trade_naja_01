const { MongoClient } = require("mongodb");
const uri = 'mongodb://localhost:27017/tradedb';  // mongodb://localhost - will fail

(async function() {
  try {

    const client = await MongoClient.connect(uri,{ useNewUrlParser: true });

    client.close();
  } catch(e) {
    console.error(e)
  }

})()