const express = require("express");
const bodyParser = require("body-parser");

// configuration
let app = express();
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  next();
});

let routes = require("./api/routes");

// routes
app.use("/", routes);

app.get("/", (req, res) => {
  const object = [
    {
      team: "CEO/CTO/COO",
      name: "FTT",
      responsibility: "Everything"
    },
    {
      team: "Biz",
      name: "RDP",
      responsibility: "Business and Planning"
    },
    {
      team: "BE",
      name: "PTC",
      responsibility: "Programming"
    }
  ];
  res.send(object);
});

// error handling

// server
let port = process.env.PORT || 8080;
app.listen(port, () => {
  console.log(`API is ready on http://localhost:${port}`);
});
